package main

import (
	"fmt"
	"math/rand"
	"time"
)

var checkCount uint
var moveCount uint

func main() {
	n := 10
	rand.Seed(time.Now().UTC().UnixNano())
	ints := make([]int, 0, n)
	for i := 0; i < n; i++ {
		ints = append(ints, rand.Int()%100)
	}

	fmt.Printf("start : %v\n", ints)
	sortedInts := insertionSort(ints)
	fmt.Printf("sorted: %v\n", sortedInts)
	fmt.Printf("checks: %d\nmoves: %d\n", checkCount, moveCount)

	added := rand.Int() % 100
	fmt.Printf("\nadd element : %d\n", added)
	fmt.Printf("start : %v\n", ints)
	sortedInts = addAndSort(added, ints)
	fmt.Printf("sorted: %v\n", sortedInts)
	fmt.Printf("checks: %d\nmoves: %d\n", checkCount, moveCount)

	remove := n / 2
	fmt.Printf("\nremove element index : %d\n", remove)
	fmt.Printf("start : %v\n", ints)
	sortedInts = removeAndSort(remove, ints)
	fmt.Printf("sorted: %v\n", sortedInts)
	fmt.Printf("checks: %d\nmoves: %d", checkCount, moveCount)
}

func insertionSort(a []int) []int {
	for i := 1; i < len(a); i++ {
		n := a[i]
		j := i
		checkCount++
		for j > 0 && a[j-1] > n {
			moveCount++
			a[j] = a[j-1]
			j--
		}
		a[j] = n
	}
	return a
}

func addAndSort(add int, a []int) []int {
	return insertionSort(append(a, add))
}

func removeAndSort(remove int, a []int) []int {
	if remove >= len(a) || remove < 0 {
		panic("wrong remove index")
	}
	return insertionSort(append(a[:remove], a[remove+1:]...))
}
